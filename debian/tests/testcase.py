class AClassWithoutANewLine(object):
    def a_method(self):
        return 'a_value'


class AClassWithoutANewLineProperty(object):
    @property
    def a_method(self):
        return 'a_value'


class AClassWithANewLine(object):

    def a_method(self):
        return 'a_value'


class AClassWithANewLineProperty(object):

    @property
    def a_method(self):
        return 'a_value'
