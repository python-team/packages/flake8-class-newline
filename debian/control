Source: flake8-class-newline
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Jose Luis Rivero <jrivero@osrfoundation.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-flake8,
               python3-pytest,
               python3-setuptools,
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/flake8-class-newline
Vcs-Git: https://salsa.debian.org/python-team/packages/flake8-class-newline.git
Rules-Requires-Root: no
Homepage: https://github.com/AlexanderVanEck/flake8-class-newline

Package: python3-flake8-class-newline
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Enhances: flake8
Description: flake8 extension to lint for a method newline after a Class definition
 PEP8 recommends surrounding every class method with a single blank line. See
 https://www.python.org/dev/peps/pep-0008/#blank-lines However flake8 is
 ambiguous about the first method having a blank line above it.
 .
 This plugin was made to enforce the blank line just after the method
 declaration.
